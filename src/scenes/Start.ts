import Phaser from 'phaser';

export default class Start extends Phaser.Scene {
  constructor() {
    super('start');
  }
  tittle?: Phaser.GameObjects.Image
  screenCenterX?:number;
  screenCenterY?:number;
  preload() {
    this.load.image('start-tittle', 'assets/start-tittle.png');
    this.load.image('button','assets/button.png');
    this.load.image('start-bg','assets/start-bg.png');
   
  }

  create() {
    this.screenCenterX = this.cameras.main.worldView.x + this.cameras.main.width / 2;
    this.screenCenterY = this.cameras.main.worldView.y + this.cameras.main.height / 2;
    this.add.image(this.screenCenterX,this.screenCenterY,'start-bg').setZ(-1).setDisplaySize(this.cameras.main.width,this.cameras.main.height)
   
   const image= this.add.image( -600, this.screenCenterY-20 ,'start-tittle').setScale(1)
   
    this.tweens.add({
      targets: image,
      x: this.screenCenterX+40,
      duration: 2000,
      ease: 'linear'
  });
  
  const buttonStart = this.physics.add.sprite(this.screenCenterX, 520,'button').setScale(0.5).setTint(0xff00ff).setAlpha(0).setInteractive()
  const textYes = this.add.text(this.screenCenterX-48, 510, 'START').setScale(2).setAlpha(0);

    this.tweens.addCounter({
      delay:4000,
      from: 0,
      to: 1,
      duration: 3000,
      onUpdate: (tween) => {
          const v = tween.getValue();
          buttonStart?.setAlpha(v)
          textYes?.setAlpha(v)
      }
  });

  buttonStart.on('pointerdown', () =>
{
  this.scene.start('GameScene')

});

  }

  update(time: number, delta: number): void {
    
  }
 
}
