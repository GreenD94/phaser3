import Phaser from 'phaser';

export default class Victory extends Phaser.Scene {
  constructor() {
    super('Victory');
  }
  hero?:Phaser.Types.Physics.Arcade.SpriteWithDynamicBody;
  ally?:Phaser.Types.Physics.Arcade.SpriteWithDynamicBody;
  buttonYes?:Phaser.Types.Physics.Arcade.SpriteWithDynamicBody;
  textYes?:Phaser.GameObjects.Text
  screenCenterX?:number;
  screenCenterY?:number;
  timedEvent?:Phaser.Time.TimerEvent
  question?: Phaser.GameObjects.Image
  
  preload() {
    this.load.spritesheet('hero-walk-right', 'assets/hero-walk-right.png', { frameWidth: 64, frameHeight: 60,  });
    this.load.spritesheet('ally-walk-left', 'assets/ally-walk-left.png', { frameWidth: 64, frameHeight: 55,  });
    this.load.spritesheet('hero-dance', 'assets/hero-dance.png', { frameWidth: 64, frameHeight: 55,  });
    this.load.spritesheet('ally-dance', 'assets/ally-dance.png', { frameWidth: 64, frameHeight: 55,  });
    this.load.image('question','assets/question.png');
    this.load.image('button','assets/button.png');
    this.load.image('victory-bg','assets/victory-bg.png');

  }

  create() {
    this.screenCenterX = this.cameras.main.worldView.x + this.cameras.main.width / 2;
    this.screenCenterY = this.cameras.main.worldView.y + this.cameras.main.height / 2;
    this.add.image(this.screenCenterX,this.screenCenterY,'victory-bg').setZ(-1).setDisplaySize(this.cameras.main.width,this.cameras.main.height)

    this.anims.create({
      key: 'walk-right',
      frames: this.anims.generateFrameNumbers('hero-walk-right', { frames: [ 0, 1, 2, 3,4,5,6,7 ] }),
      frameRate: 5,
      repeat: -1
    });
    this.anims.create({
      key: 'idle',
      frames: this.anims.generateFrameNumbers('hero-walk-right', { frames: [ 0 ] }),
      frameRate: 5,
    });


    this.anims.create({
      key: 'ally-walk-left',
      frames: this.anims.generateFrameNumbers('ally-walk-left', { frames: [ 0, 1, 2, 3,4,5,6,7,8,9,10 ] }),
      frameRate: 7,
      repeat: -1
    });

    this.anims.create({
      key: 'ally-idle',
      frames: this.anims.generateFrameNumbers('ally-walk-left', { frames: [ 0 ] }),
      frameRate: 7,
      repeat: -1
    });


    this.anims.create({
      key: 'hero-dance',
      frames: this.anims.generateFrameNumbers('hero-dance', { frames: [ 0,1,2,3,4,5,6,7,8,9 ] }),
      frameRate: 7,
      repeat: -1
    });

    this.anims.create({
      key: 'ally-dance',
      frames: this.anims.generateFrameNumbers('ally-dance', { frames: [ 0,1,2,3,4,5,6,7,8,9 ] }),
      frameRate: 7,
      repeat: -1
    });

  this.buttonYes = this.physics.add.sprite(720, 530,'button').setScale(0.5).setTint(0xff00ff).setAlpha(0).setInteractive()
  this.textYes = this.add.text(698, 515, 'Si').setScale(2).setAlpha(0);


  this.hero = this.physics.add.sprite(600, 370,'hero-walk-right')
  this.hero.setScale(2);
  this.hero.play('walk-right');
  this.hero.setPosition(-100,this.screenCenterY+100);

  this.ally = this.physics.add.sprite( this.cameras.main.width+100,  this.screenCenterY+100,'ally-walk-left')
  this.ally.setScale(2);
  this.ally.play('ally-walk-left');

 this.question= this.add.image(this.screenCenterX, 100,'question').setAlpha(0);
this.buttonYes.on('pointerdown', () =>
{
    this.question?.setPosition(-400,-400);
    this.buttonYes?.setPosition(-400,-400);
    this.textYes?.setPosition(-400,-400);

    this.hero?.play('hero-dance')
    this.ally?.play('ally-dance')

});

  this.tweens.addCounter({
    delay:4000,
    from: 0,
    to: 1,
    duration: 3000,
    onUpdate: (tween) => {
        const v = tween.getValue();
        this.question?.setAlpha(v);
        this.buttonYes?.setAlpha(v)
        this.textYes?.setAlpha(v)
    }
});


  this.add.tween({
    targets: this.hero,
    x: 300,
    duration: 4000,
    onComplete: () => {
    this.hero?.play('idle')
  },
    ease: 'Linear',
  });

  this.add.tween({
    targets: this.ally,
    x: 500,
    duration: 4000,
    onComplete: () => {
    this.ally?.play('ally-idle')
  },
    ease: 'Linear',
  });

  }

  update(time: number, delta: number): void {
  } 
}
