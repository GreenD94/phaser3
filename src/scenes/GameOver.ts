import Phaser from 'phaser';

export default class GameOver extends Phaser.Scene {
  constructor() {
    super('GameOver');
  }
  hero?:Phaser.GameObjects.Sprite;
  screenCenterX?:number;
  screenCenterY?:number;
  preload() {
    this.load.spritesheet('hero-die', 'assets/hero-die.png', { frameWidth: 64, frameHeight: 55,  });
    this.load.image('gameover','assets/gameover.png')
  }

  create() {
    this.screenCenterX = this.cameras.main.worldView.x + this.cameras.main.width / 2;
    this.screenCenterY = this.cameras.main.worldView.y + this.cameras.main.height / 2;


    this.anims.create({
      key: 'hero-die',
      frames: this.anims.generateFrameNumbers('hero-die', { frames: [ 0, 1, 2, 3,4,5,6,7 ] }),
      frameRate: 2,
    });
  this.hero = this.add.sprite(    this.screenCenterX+20, this.screenCenterY+150,'hero-die')
  this.hero.setScale(2);
  this.hero.play('hero-die');

  this.add.image(this.screenCenterX,this.screenCenterY,'gameover').setDisplaySize(200,200)


  }

  update(time: number, delta: number): void {
   
  }
}
