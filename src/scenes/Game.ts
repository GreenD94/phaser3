import Phaser from 'phaser';

export default class Demo extends Phaser.Scene {
  constructor() {
    super('GameScene');
  }
  hero?:Phaser.Types.Physics.Arcade.SpriteWithDynamicBody;
  enemy?:Phaser.Types.Physics.Arcade.SpriteWithDynamicBody;
  enemy2?:Phaser.Types.Physics.Arcade.SpriteWithDynamicBody;
  enemy3?:Phaser.Types.Physics.Arcade.SpriteWithDynamicBody;
  screenCenterX?:number;
  screenCenterY?:number;
  timedEvent?:Phaser.Time.TimerEvent
  timedEvent2?:Phaser.Time.TimerEvent
  timedEvent3?:Phaser.Time.TimerEvent
  winTimedEvent?:Phaser.Time.TimerEvent
  text?:Phaser.GameObjects.Text
  
  preload() {
    this.load.spritesheet('hero-walk-down', 'assets/hero-walk-down.png', { frameWidth: 64, frameHeight: 60,  });
    this.load.spritesheet('kicking-cat', 'assets/kicking-cat.png', { frameWidth: 50, frameHeight: 40,  });
    this.load.image('game-bg','assets/game-bg.png');
  }

  create() {
    this.screenCenterX = this.cameras.main.worldView.x + this.cameras.main.width / 2;
    this.screenCenterY = this.cameras.main.worldView.y + this.cameras.main.height / 2;
    this.add.image(this.screenCenterX,this.screenCenterY,'game-bg').setZ(-1).setDisplaySize(this.cameras.main.width,this.cameras.main.height)
    this.anims.create({
      key: 'walk',
      frames: this.anims.generateFrameNumbers('hero-walk-down', { frames: [ 0, 1, 2, 3,4,5,6,7 ] }),
      frameRate: 5,
      repeat: -1
    });

    this.anims.create({
      key: 'kicking-cat',
      frames: this.anims.generateFrameNumbers('kicking-cat', { frames: [ 0, 1, 2, 3,4,5,6,7,8,9,10 ] }),
      frameRate: 7,
      repeat: -1
    });


  this.hero = this.physics.add.sprite(600, 370,'hero-walk-down').setCollideWorldBounds(true);
  this.hero.setScale(2);
  this.hero.play('walk');
  this.hero.setPosition(this.screenCenterX,60);
  this.hero.setMaxVelocity(300)
  this.hero.setBodySize(35,50).body.offset.setTo(2,5)

  this.enemy = this.physics.add.sprite( this.screenCenterX,  this.cameras.main.height,'kicking-cat')
  this.enemy.setScale(2);
  this.enemy.play('kicking-cat');
  this.enemy.setVisible(false)
  this.enemy.setBodySize(20,30).body.offset.setTo(2,5)

  this.physics.add.collider(this.enemy,this.hero,()=>this.scene.start('GameOver'));
  this.timedEvent = this.time.addEvent({ delay: 1000, callback: this.moveevent, callbackScope: this, repeat: 1 });

  this.winTimedEvent = this.time.addEvent({ delay: 4000, callback: (x:any)=>{
    if(this?.winTimedEvent?.repeatCount==9){
      this.enemy2 = this.physics.add.sprite( this.screenCenterX??0,  this.cameras.main.height,'kicking-cat')
      this.enemy2.setScale(2).setBounce(1);
      this.enemy2.play('kicking-cat');
      this.enemy2.setVisible(false).setCollideWorldBounds(true)
      this.enemy2.setBodySize(20,30).body.offset.setTo(2,5)

      this.physics.add.collider(this.enemy2,this.hero as Phaser.Types.Physics.Arcade.SpriteWithDynamicBody ,()=>this.scene.start('GameOver'));
      this.timedEvent2 = this.time.addEvent({ delay: 1000, callback: this.moveevent2, callbackScope: this, repeat: 1 });
    }
    if(this?.winTimedEvent?.repeatCount==8){
      this.enemy3 = this.physics.add.sprite( this.screenCenterX??0,  this.cameras.main.height,'kicking-cat')
      this.enemy3.setScale(2).setBounce(1);
      this.enemy3.play('kicking-cat');
      this.enemy3.setVisible(false).setCollideWorldBounds(true)
      this.enemy3.setBodySize(20,30).body.offset.setTo(2,5)

      this.physics.add.collider(this.enemy3,this.hero as Phaser.Types.Physics.Arcade.SpriteWithDynamicBody ,()=>this.scene.start('GameOver'));
      this.timedEvent2 = this.time.addEvent({ delay: 1000, callback: this.moveevent3, callbackScope: this, repeat: 1 });
    }
    if(this?.winTimedEvent?.repeatCount==0)this.scene.start('Victory');;
  }, callbackScope: this, repeat: 10 });

  this.text = this.add.text( this.screenCenterX, this.screenCenterY, 'Ronda 1');

  }

  update(time: number, delta: number): void {
    this.text?.setText(`Ronda:  ${this.winTimedEvent?.repeatCount}`);

    const centerx=this.hero?.x??0;
    const factor= ((this.input.mousePointer.x-centerx)>0)?1:-1;
    this.hero?.setAcceleration((factor*600),0); 
    const enemyWidth=this.enemy?.y??0
    if(enemyWidth<-50){
      this.enemy?.setPosition(this.screenCenterX,  this.cameras.main.height)
      this.enemy?.setVisible(false)
      this.enemy?.setAcceleration(0);
      this.enemy?.setVelocity(0);
      this.timedEvent?.remove();
      this.time.removeEvent(this.timedEvent as Phaser.Time.TimerEvent );
      this.timedEvent = this.time.addEvent({ delay: 1000, callback: this.moveevent, callbackScope: this, repeat: 1 });
    } 
  }
  moveevent(){
    this.enemy?.setVisible(true)
    this.enemy?.setAcceleration(0,-Phaser.Math.Between(100,400));
    this.enemy?.setVelocityX(Phaser.Math.Between(-100,100));
  }
  moveevent2(){
    this.enemy2?.setVisible(true)
    this.enemy2?.setAcceleration(0,-Phaser.Math.Between(100,400));
    this.enemy2?.setVelocityX(Phaser.Math.Between(-100,100));
  }
  moveevent3(){
    this.enemy3?.setVisible(true)
    this.enemy3?.setAcceleration(20,-Phaser.Math.Between(100,400));
    this.enemy3?.setVelocityX(Phaser.Math.Between(-100,100));
  }

  chngeToGameOverScene(){
    this.scene.start('GameOver');
  }
}
