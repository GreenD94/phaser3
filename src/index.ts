import Phaser from 'phaser';
import config from './config';
import GameScene from './scenes/Game';
import GameOver from './scenes/GameOver';
import Victory from './scenes/Victory';
import Start from './scenes/Start';

new Phaser.Game(
  Object.assign(config, {
    scene: [Start,GameScene,GameOver,Victory]
  })
);

